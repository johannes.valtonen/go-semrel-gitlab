---
title: "Get started"
anchor: "get-started"
description: The steps set up go-semrel-gitlab
weight: 30
---

# Getting started

1. Start [formatting commit messages](../commit-message) so that `go-semrel-gitlab` understands them
   - first line of form: `type(scope): subject`
   - breaking change marked with: `BREAKING CHANGE:`
1. Give `go-semrel-gitlab` permission to create tags and commit in your project
  - Create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with api scope
  - Assign it to GL_TOKEN in your project 'Settings → CI/CD → Variables'
1. Use `go-semrel-gitlab` in Gitlab CI jobs, for example to
  - [update changelog and make a release](https://gitlab.com/juhani/go-semrel-gitlab/blob/4b1ea99782d6abb99b56c666ed4cc5eb5c22748f/.gitlab-ci.yml#L148-150)
    (update changelog, commit version bump, create tag, and attach download to tag description)
  - [determine the next version number](https://gitlab.com/juhani/go-semrel-gitlab/blob/4b1ea99782d6abb99b56c666ed4cc5eb5c22748f/.gitlab-ci.yml#L29-38),
    to be embedded into the executable in 
    [subsequent step of](https://gitlab.com/juhani/go-semrel-gitlab/blob/4b1ea99782d6abb99b56c666ed4cc5eb5c22748f/.gitlab-ci.yml#L44) the pipeline.
